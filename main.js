/*************/
// StackOverflow code to convert from youtube api timestamps to h:m:s, seems to work
/*************/
function ConvertTime(duration) {
    var a = duration.match(/\d+/g);

    if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
        a = [0, a[0], 0];
    }

    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
        a = [a[0], 0, a[1]];
    }
    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
        a = [a[0], 0, 0];
    }

    duration = 0;

    if (a.length == 3) {
        duration = duration + parseInt(a[0]) * 3600;
        duration = duration + parseInt(a[1]) * 60;
        duration = duration + parseInt(a[2]);
    }

    if (a.length == 2) {
        duration = duration + parseInt(a[0]) * 60;
        duration = duration + parseInt(a[1]);
    }

    if (a.length == 1) {
        duration = duration + parseInt(a[0]);
    }
    var h = Math.floor(duration / 3600);
    var m = Math.floor(duration % 3600 / 60);
    var s = Math.floor(duration % 3600 % 60);
    return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") + m + ":" + (s < 10 ? "0" : "") + s);
}

/*************/
// YouTube Player Init
/*************/
function ResizeYoutubePlayer () {
    var width = $(".video-player-module .module-wrapper").width();
    var height = width * 0.5625;

    $(".video-player-module .module-wrapper").height(height);
}

var playlistApiUrl = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&maxResults=50&key=REMOVEDFORSHARING"
var videoApiUrl = "https://www.googleapis.com/youtube/v3/videos?key=REMOVEDFORSHARING&part=snippet,contentDetails&id="

function HandleApiResponse (playlistData, region, activeVidId) {
    for (var i = 0; i < playlistData.items.length; i++) {
        var url = videoApiUrl + playlistData.items[i].contentDetails.videoId;
        $.ajax({
            type: 'GET',
            url: url,
            success: function(videoData) {
                if (videoData.items.length > 0 && i < 5) {
                    CreateVideoThumbnail($(".video-list." + region), videoData.items[0], activeVidId);
                }
            },
            async: false // Can't make this async without losing order of thumbnails, without a lot more work
        });
    }
}

function onPlayerReady () {
    var lang = "%%=v(@LANG)=%%";
    var plEN = {
        "fulldoc": "PLlwyQnKvoqbax7z67Gc1aZCcUfAbz3msy",
        "central": "PLlwyQnKvoqbYd8WePmcZqWO5_DE9OT5Si",
        "rockies": "PLlwyQnKvoqbaLnhEsaivkDTCCQJIRQ0aw",
        "maritime": "PLlwyQnKvoqbYhzA2R3iNdxoqAmYQugMTT"
    };
    var plFR = {
        "fulldoc": "PLlwyQnKvoqbax7z67Gc1aZCcUfAbz3msy",
        "central": "PLlwyQnKvoqbaxlILZBGWTmGlYeA26fkOA",
        "rockies": "PLlwyQnKvoqbaio6n1WF-Tw7nobcQUgkvm",
        "maritime": "PLlwyQnKvoqbb5Mq3YBzxrdpC1A_lBTr4-"
    };

    var playlists = (lang == 'f') ? plFR : plEN;

    $.each(playlists, function(region, playlistID) {
        $.ajax({
            type: 'GET',
            url: playlistApiUrl + playlistID,
            success: function(response) {
                var firstVidId = "";
                if (region == "fulldoc") {
                    firstVidId = response.items[0].snippet.resourceId.videoId;
                    player.cueVideoById(firstVidId);
                }
                HandleApiResponse(response, region, firstVidId);
            }
        });
    });

    ResizeYoutubePlayer();
}

function onStateChange (event) {
    var $nowPlaying = $(".video-list li.active");
    if (event.data ==- YT.PlayerState.ENDED && !$nowPlaying.is(":last-child")) {
        var vidId = $nowPlaying.removeClass("active").next().addClass("active").attr("data-vid-id");
        player.loadVideoById(vidId);
    }
}

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: "100%",
        width: "100%",
        videoId: "",
        events: {
            "onReady": onPlayerReady,
            "onStateChange": onStateChange
        }
    });
}

/*************/
// Everything else
/*************/
function PlayNewVideo (event) {
    player.loadVideoById(event.currentTarget.attributes["data-vid-id"].nodeValue);
    $(".video-list li").removeClass("active");
    $(event.currentTarget).addClass("active");
}

function CreateVideoThumbnail(target, data, activeVidId) {
    var $videoItem =
        $("<li>")
            .addClass((data.id == activeVidId) ? "active" : "")
            .attr("data-vid-id", data.id)
            .append(
                $("<div>")
                    .addClass("overlays")
                    .append(
                        $("<div>")
                            .addClass("now-playing")
                            .append(
                                $("<span>")
                                    .text("%%=v(@nowPlaying)=%%")
                            )
                    )
                    .append(
                        $("<div>")
                            .addClass("play-button")
                    )
                    .append(
                        $("<span>")
                            .addClass("vid-time")
                            .text(ConvertTime(data.contentDetails.duration))
                    )
                    .append(
                        $("<img>")
                            .attr("src", data.snippet.thumbnails.medium.url)
                    )
            )
            .append(
                $("<div>")
                    .addClass("vid-text")
                    .append(
                        $("<span>")
                            .addClass("vid-title")
                            .text(data.snippet.title.replace("#HDCommonGround | ", "").replace("#HDLaRoutePartagée | ", "").replace("#HDLaRoutePartagee | ", ""))
                    )
            );

    $videoItem.on("click", PlayNewVideo);

    target.append($videoItem);
}

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function SwitchListRegion (event) {
    $(".video-region-picker li").removeClass("active");
    $(".video-list").removeClass("active");
    $(event.currentTarget).addClass("active");
    $(".video-list." + event.currentTarget.attributes["data-region"].nodeValue).addClass("active");
    if (player.getPlayerState() !== 1) {
        $(".video-list li").removeClass("active");
        player.cueVideoById($(".video-list.active").children().first().addClass("active").attr("data-vid-id"))
    }
};

$(".video-region-picker li").on("click", SwitchListRegion);

function ScrollStuff () {
    var scroll = $(window).scrollTop();
    var heroHeight = $(".hero").height();
    var topPercent = 50;
    var navHeight = $(".black-bar").innerHeight();

    // $(".bg-vid video").css("top", + String(topPercent + scroll/25) + "%");

    if (scroll >= $(".header-container").position().top) {
        $('.header').addClass("show");
    } else {
        $('.header').removeClass("show");
    }

    if (scroll >= $(".meet-riders-module").position().top - navHeight) {
        $(".header li").removeClass("active");
        $("#navRiders").addClass("active");
    } else {
        $("#navRiders").removeClass("active");
    }

    if (scroll >= $(".bikes-module").position().top - navHeight) {
        $(".header li").removeClass("active");
        $("#navBikes").addClass("active");
    } else {
        $("#navBikes").removeClass("active");
    }

    if (scroll >= $(".try-one-module").position().top - navHeight) {
        $(".header li").removeClass("active");
        $("#navTestRide").addClass("active");
    } else {
        $("#navTestRide").removeClass("active");
    }

    if (scroll >= $(".keep-up-module").position().top - navHeight) {
        $(".header li").removeClass("active");
        $("#navEmail").addClass("active");
    } else {
        $("#navEmail").removeClass("active");
    }
}

$(window).scroll(ScrollStuff);

$(".hero .button").click(function() {
    $('html, body').animate({
        scrollTop: $(".header-container").offset().top + 2
    }, 500);
});

$(".cg-logo").click(function() {
    var navHeight = $(".black-bar").innerHeight();
    $('html, body').animate({
        scrollTop: 0
    }, 500);
    $(".hamburger").removeClass("is-active");
    $(".header").removeClass("expanded");
});

$("#navRiders").click(function() {
    var navHeight = $(".black-bar").innerHeight();
    $('html, body').animate({
        scrollTop: $(".meet-riders-module").offset().top - navHeight + 2
    }, 500);
    $(".hamburger").removeClass("is-active");
    $(".header").removeClass("expanded");
});

$("#navBikes").click(function() {
    var navHeight = $(".black-bar").innerHeight();
    $('html, body').animate({
        scrollTop: $(".bikes-module").offset().top - navHeight + 2
    }, 500);
    $(".hamburger").removeClass("is-active");
    $(".header").removeClass("expanded");
});

$("#navTestRide").click(function() {
    var navHeight = $(".black-bar").innerHeight();
    $('html, body').animate({
        scrollTop: $(".try-one-module").offset().top - navHeight + 2
    }, 500);
    $(".hamburger").removeClass("is-active");
    $(".header").removeClass("expanded");
});

$("#navEmail").click(function() {
    var navHeight = $(".black-bar").innerHeight();
    $('html, body').animate({
        scrollTop: $(".keep-up-module").offset().top - navHeight + 2
    }, 500);
    $(".hamburger").removeClass("is-active");
    $(".header").removeClass("expanded");
});

var lastSize = 0;
function FitHeroContent () {
    var barsHeight = $(".black-bar").height() * 2;
    var heroHeight = $(".hero").height();
    var contentHeight = $(".hero-content-wrapper").innerHeight();
    var fontSize = parseInt($(".hero").css("font-size"));
    var viewSize = heroHeight - barsHeight - 30;

    if (lastSize >= heroHeight) {
        while (viewSize < contentHeight) {
            barsHeight = $(".black-bar").height() * 2;
            heroHeight = $(".hero").height();
            contentHeight = $(".hero-content-wrapper").innerHeight();
            viewSize = heroHeight - barsHeight - 30;
            fontSize -= 0.1;
            $(".hero").css("font-size", String(fontSize) + "px");
        }
    }

    if (lastSize <= heroHeight) {
        while (viewSize > contentHeight) {
            barsHeight = $(".black-bar").height() * 2;
            heroHeight = $(".hero").height();
            contentHeight = $(".hero-content-wrapper").innerHeight();
            viewSize = heroHeight - barsHeight - 30;
            fontSize += 0.1;
            $(".hero").css("font-size", String(fontSize) + "px");
        }
    }

    lastSize = heroHeight;
}

$(window).resize(function() {
    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function(){
        FitHeroContent();
        ResizeYoutubePlayer();
    }, 50);
});

$(".hamburger").click(function() {
    if ($(this).hasClass("is-active")) {
        $(this).removeClass("is-active");
        $(".header").removeClass("expanded");
    } else {
        $(this).addClass("is-active");
        $(".header").addClass("expanded");
    }
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function CheckUrlParams () {
    var scrollTarget = getParameterByName('target')
    if (scrollTarget === "video") {
        $('html, body').animate({
            scrollTop: $(".header-container").offset().top + 2
        }, 500);
    }
}

$(document).ready(function() {
    lastSize = $(".hero").height();
    FitHeroContent();
    setTimeout(FitHeroContent, 1000);
    ScrollStuff();
    CheckUrlParams();
});
